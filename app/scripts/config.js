'use strict';

var AppSettings = {
  appTitle: 'Example Application',
  apiUrl: '/'
};

module.exports = AppSettings;
